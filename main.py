import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server

########MY DATA#########
flights = pd.read_csv("flights.csv.gz")
airports = pd.read_csv("airports.csv")

#combine flights and airport info
df_f = flights.merge(airports, 
                     left_on='dest',
                     right_on='faa',
                     how = "left")

# count number of flights to state
state_agg = df_f.groupby(['state']).size().to_frame(name = 'count').reset_index()


# sum the total arrival and dept. delays 
state_delay_agg = df_f.groupby('state').agg({'dep_delay':'sum','arr_delay':'sum'})
#combine with counts
state_delay_agg = state_delay_agg.merge(state_agg, 
                     left_on='state',
                     right_on='state',
                     how = "left")
# Create ratio of delay per flight
state_delay_agg['Dep. Delay per flight (minutes)'] = state_delay_agg['dep_delay']/state_delay_agg['count']
#create ratio of arrival delay per flight 
state_delay_agg['Arr. Delay per flight (minutes)'] = state_delay_agg['arr_delay']/state_delay_agg['count']

#fig 1 is the departure delay...
fig1 = px.choropleth(state_delay_agg, 
                    locations = 'state',
                    locationmode = 'USA-states',
                    color = 'Dep. Delay per flight (minutes)',
                    scope = 'usa')

#fig2 is the arrival delay
fig2 = px.choropleth(state_delay_agg, 
                    locations = 'state',
                    locationmode = 'USA-states',
                    color = 'Arr. Delay per flight (minutes)',
                    scope = 'usa')



# # assume you have a "long-form" data frame
# # see https://plotly.com/python/px-arguments/ for more options
# df = pd.DataFrame({
#     "Fruit": ["Apples", "Oranges", "Bananas", "Apples", "Oranges", "Bananas"],
#     "Amount": [4, 1, 2, 2, 4, 5],
#     "City": ["SF", "SF", "SF", "Montreal", "Montreal", "Montreal"]
# })

# fig = px.bar(df, x="Fruit", y="Amount", color="City", barmode="group")

# df2 = pd.read_csv('https://gist.githubusercontent.com/chriddyp/5d1ea79569ed194d432e56108a04d188/raw/a9f9e8076b837d541398e999dcbac2b2826a81f8/gdp-life-exp-2007.csv')

# fig2 = px.scatter(df2, x="gdp per capita", y="life expectancy",
#                  size="population", color="continent", hover_name="country",
#                  log_x=True, size_max=60)

app.layout = html.Div(children=[
    html.H1(children='Hello 507 GRADERS'),

    html.Div(children='''
        Figure 1 shows the average departure delay for flights into a given state, from either EWR, JFK, or LGA airports. 
    '''),

    dcc.Graph(
        id='dep-graph',
        figure=fig1
    ),

    html.Div(children='''
        Figure 2 shows the average arrival delay for flights into a given state, from either EWR, JFK, or LGA airports. 
        In particular, Wyoming and Oklahoma seem to be bottlenecks with long (~20 minute) delays in arrival and departures. 
    '''),

    dcc.Graph(
        id='arr-graph',
        figure=fig2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
